﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Connect4
{
    class Connect4
    {
    public static int PlayerDrop(char[,] board, playerInfo activePlayer)
    {
        int dropChoice;

        Console.WriteLine(activePlayer.playerName + "'s Turn ");
        do
        {
            Console.WriteLine("Please enter a number between 1 and 7: ");
            dropChoice = Convert.ToInt32(Console.ReadLine());
        } while (dropChoice < 1 || dropChoice > 7);

        while (board[1, dropChoice] == 'R' || board[1, dropChoice] == 'Y')
        {
            Console.WriteLine("That row is full, please enter a new row: ");
            dropChoice = Convert.ToInt32(Console.ReadLine());
        }

        return dropChoice;
    }

    public static void CheckBellow(char[,] board, playerInfo activePlayer, int dropChoice)
    {
        int length, turn;
        length = 6;
        turn = 0;

        do
        {
            if (board[length, dropChoice] != 'R' && board[length, dropChoice] != 'Y')
            {
                board[length, dropChoice] = activePlayer.playerID;
                turn = 1;
            }
            else
                --length;
        } while (turn != 1);


    }

    public static void DisplayBoard(char[,] board)
    {
        int rows = 6, columns = 7, i, ix;

        for (i = 1; i <= rows; i++)
        {
            Console.Write("|");
            for (ix = 1; ix <= columns; ix++)
            {
                if (board[i, ix] != 'R' && board[i, ix] != 'Y')
                    board[i, ix] = '-';

                Console.Write(board[i, ix]);

            }

            Console.Write("| \n");
        }

    }

    public static int CheckFour(char[,] board, playerInfo activePlayer)
    {
        char XO;
        int win;

        XO = activePlayer.playerID;
        win = 0;

        for (int i = 8; i >= 1; --i)
        {

            for (int ix = 9; ix >= 1; --ix)
            {

                if (board[i, ix] == XO &&
                    board[i - 1, ix - 1] == XO &&
                    board[i - 2, ix - 2] == XO &&
                    board[i - 3, ix - 3] == XO)
                {
                    win = 1;
                }


                if (board[i, ix] == XO &&
                    board[i, ix - 1] == XO &&
                    board[i, ix - 2] == XO &&
                    board[i, ix - 3] == XO)
                {
                    win = 1;
                }

                if (board[i, ix] == XO &&
                    board[i - 1, ix] == XO &&
                    board[i - 2, ix] == XO &&
                    board[i - 3, ix] == XO)
                {
                    win = 1;
                }

                if (board[i, ix] == XO &&
                    board[i - 1, ix + 1] == XO &&
                    board[i - 2, ix + 2] == XO &&
                    board[i - 3, ix + 3] == XO)
                {
                    win = 1;
                }

                if (board[i, ix] == XO &&
                     board[i, ix + 1] == XO &&
                     board[i, ix + 2] == XO &&
                     board[i, ix + 3] == XO)
                {
                    win = 1;
                }
            }

        }

        return win;
    }

    public static int FullBoard(char[,] board)
    {
        int full;
        full = 0;
        for (int i = 1; i <= 7; ++i)
        {
            if (board[1, i] != '-')
                ++full;
        }

        return full;
    }

    public static void PlayerWin(playerInfo activePlayer)
    {
        Console.WriteLine(activePlayer.playerName + " Connected Four, You Win!");
    }

    public static int restart(char[,] board)
    {
        int restart;

        Console.WriteLine("Would you like to restart? Yes(1) No(2): ");
        restart = Convert.ToInt32(Console.ReadLine());
        if (restart == 1)
        {
            for (int i = 1; i <= 6; i++)
            {
                for (int ix = 1; ix <= 7; ix++)
                {
                    board[i, ix] = '-';
                }
            }
        }
        else
            Console.WriteLine("Goodbye!");
        return restart;
    }


}
}

