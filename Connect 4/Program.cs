﻿using System;


    public struct playerInfo
    {
        public String playerName;
        public char playerID;
    };

    namespace Connect4
    {
        class Program
        {
            static void Main(string[] args)
            {
                playerInfo playerOne = new playerInfo();
                playerInfo playerTwo = new playerInfo();
                char[,] board = new char[9, 10];
                int dropChoice, win, full, again;

                Console.WriteLine("Let's Play Connect 4");
                
                playerOne.playerName = "red player";
                playerOne.playerID = 'R';
                
                playerTwo.playerName = "yellow player";
                playerTwo.playerID = 'Y';

                full = 0;
                win = 0;
                again = 0;
            
                Connect4.DisplayBoard(board);
                do
                {
                    dropChoice = Connect4.PlayerDrop(board, playerOne);
                    Connect4.CheckBellow(board, playerOne, dropChoice);
                    Connect4.DisplayBoard(board);
                    win = Connect4.CheckFour(board, playerOne);
                    if (win == 1)
                    {
                        Connect4.PlayerWin(playerOne);
                        again = Connect4.restart(board);
                        if (again == 2)
                        {
                            break;
                        }
                    }

                    dropChoice = Connect4.PlayerDrop(board, playerTwo);
                    Connect4.CheckBellow(board, playerTwo, dropChoice);
                   Connect4.DisplayBoard(board);
                    win = Connect4.CheckFour(board, playerTwo);
                    if (win == 1)
                    {
                        Connect4.PlayerWin(playerTwo);
                        again = Connect4.restart(board);
                        if (again == 2)
                        {
                            break;
                        }
                    }
                    full = Connect4.FullBoard(board);
                    if (full == 7)
                    {
                        
                        Console.WriteLine("The board is full, it is a draw!");
                        again = Connect4.restart(board);
                    }

                } while (again != 2);
            }
            

        }
    }
